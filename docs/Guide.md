# Setup Guide
This guide shows, documented by photos, how to setup the hardware and software as detailed as possible.

## Pi Software from Scratch
This guide explains how to install all the software on a Raspberry Pi (or another device) **without** the premade image.

### Preparations
- The Raspberry Pi must run a Linux OS (preferable Arch Linux).
- (Git must be installed) - Only if the software will be downloaded through git.
- A recent version of node.js (and npm) must be installed. (`node > 6.x.x`).
- Ffmpeg with `libmp3lame` support must be installed / compiled.
	- The exact configure command for a Raspberry Pi would be: \\TODO.
	- For a guide see [this](https://trac.ffmpeg.org/wiki/How%20to%20quickly%20compile%20FFmpeg%20with%20mp3,%20aac%2B%20and%20x264).
- A SSH keypair must be generated and exchanged with the Master-Server.
	- Run `ssh-keygen` in a terminal and with user which will run the Pi software. **Do not** use a passphrase!
	- Run `ssh-copy-id [user]@[masterserver]` with the same user.

### Installation of the Software
1. Download the software.
	- If using git run: `git clobe --depth=1 https://git.protagon.space/root/doccam-pi.git`
2. Enter the directory where the you cloned/extracted the software to.
3. Copy or move `config.js.example` to `config.js`
4. Edit `config.js` and enter the right master server and configuration details. (If you dont have a secieal ffmpeg path for the `ffmpeg-path` setting, just enter `ffmpeg`). See **[this](Usage.md#configuration-secconf)** for more info.
	- Note: B8210 Cameras need the custom flags: -b:a 128k,-ac 1,-ar 11025,-acodec libmp3lame
5. Run `npm install` to install all the depencies.
5. Set up a service to run the software in the background and restart it automaticly on crash (which shouldn't happen).
6. Done, the software should now be running. (Or at least appear on the Web Interface for further setup.)

## Pi Software Setup from the premade Image
### Requirements
- A internet connection.
- A SD-Card reader.
- A software to flash the image to the SD-Card: [try this one](https://www.etcher.io/).

### Setup
1. Aquire the Raspberry Pi image for the Pi from: \\TODO
2. Flash the image to the SD-Card of the Raspberry Pi using a Card Reader.
3. Insert the SD-Card into the Raspberry Pi and boot it up. (Connected to the network!)
4. Open the Web-Interface. A new item should appear with a name that starts with `unconfigured`.
5. Configure the Pi through the Web Interface and you're done. Remeber to **choose a unique name** for the pi. See ****[this](Usage.md#configuration-secconf)**** for more info.
	- Note: B8210 Cameras need the custom flags: -b:a 128k,-ac 1,-ar 11025,-acodec libmp3lame
6. The Pi should reappear on the Web-Interface under the new name

### Hints
- SSH tunnel user: `alarm`, Password: `secretalbatross`
- Pi software service: `sudo systemctl [start/stop/restart] cam`

## Notes on the Server software
- You can change the Port-Ranges through `conig.js`.
- To change the login creditials in the `users.htpasswd` file. (Just generate new entries.)

## Server Setup from Scratch
This guide is for users with knowledge about Linux Server Hosting.

### Preparations
- Secure Server Setup.
- Port range of 8080-8200 (more or less, depending on the number of Pis). `NUMPORTS=NUMPIS x 2`
- Node.js (`> 6.x.x`) and NPM Installed.
- NGINX for reverse proxying to the server software through https.
- A domain name: `exaple.com`
- A wildcard domain for all subdomains: `*.example.com`
- The server software cloned from: `git@git.protagon.space:root/doccam-server.git`

### Configuration
The configuration features only the selection of the server application port and the start ports for the SSH tunnels. Set the according to you preferences, but allways higher then `8000`! (Unless you are tunning the server software as root which is not recommendet!)
The server Software is smart enough to allways choose unused ports.

### Setup
1. Setup a service to run the server software. (As non-login user!)
   File to execute: `server.js`
2. Setup nginx as reverse proxy to the server.
	Example:
```
#worker_processes

events {
	worker_connections  1024;
}


http {
	include       mime.types;
	default_type  application/octet-stream;


	sendfile        on;
	keepalive_timeout  65;

	server {
		listen 80;
		listen [::]:80;
		server_name *.cams.doc.govt.nz;
		return 301 https://$host$request_uri;
	}

	server {
		listen 443 ssl http2;
		server_name cams.doc.govt.nz;
		root /var/www;

		include snippets/ssl-doc.conf;o
		include snippets/ssl-params.conf;

		location ~ /.well-known {
			allow all;
		}

		location / {
			proxy_pass http://127.0.0.1:8080/;
			proxy_set_header Host $host;
			proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			proxy_set_header Upgrade $http_upgrade;
			proxy_set_header Connection "upgrade";
		}
	}

	server {
		listen 443 ssl http2;

		include snippets/ssl-doc-sub.conf;
		include snippets/ssl-params.conf;

		server_name "~^port(?<forwarded_port>[0-9]{4,4})\.cams\.doc\.govt\.nz$";

		location ~ (\.js|profile)$ {
			if ($forwarded_port = "") {
                                return 404;
                        }
                        if ($forwarded_port) {
                                proxy_pass http://127.0.0.1:$forwarded_port;
                        }

    			add_header  Content-Type    application/x-javascript;
		}

		location / {
			if ($forwarded_port = "") {
				return 404;
			}
			if ($forwarded_port) {
				proxy_pass http://127.0.0.1:$forwarded_port;
			}
				proxy_set_header Host $host;
                        	proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
			add_header X-Frame-Options SAMEORIGIN;
		}
	}
}
```

3. Create a user for ssh access and setup the Pis ssh keys.
4. Configure the port ranges in `config.js`. Note that if you use ports with more or less then 4 digits for the camera panel, you must change the configuration file of nginx (regex). Change login creditials. (See the [notes](#hints-on-the-server-software) for more.)
5. Start everything and you're done.

## AWS Image / Current Installation
The premade AWS image features a working installation of the camera software. The web-interface is secured by ssl.

### Requirements
- A domain name: `exaple.com`
- A wildcard domain for all subdomains: `*.example.com`
- A SSL certificate for these domains (will be generated in the Guide)
- An aws `t2.micro` instance with the AMI `ami-da5e6fb9`

### Setup
1. Install the Image to AWS.
2. Configure open port ranges in the AWS panels: 80, 545333, 8080-8300
3. Generate an ssl certificate (via let's encrypt) for the root domain:
		Command: `certbot certonly --webroot -w /var/www/ -d [DOMAIN]` (Replace **[DOMAIN]** with your domain)
		Edit: `/etc/nginx/snippets/ssl-doc.conf` with the right paths.
4. Generate an ssl certificate for the subdomains:

		Command: `cd /et/ssl/self && openssl req -new -x509 -nodes -newkey rsa:4096 -keyout server.key -out server.crt  -days 1800 -subj "/C=[COUNTRY]/ST=[COMPANY/NAME]/L=[LOCATION]/O=[ISUUER]/CN=*.[DOMAIN]"`
	  Edit cronjobs with `EDITOR=nano crontab -e` to match the details of the self signet certificate (copy command).
5. Configure NGINX (`/etc/nginx/nginx.conf`) to use the domains and certificates.
6. Change login creditials in the server software folder (`/var/doccam-server`). (See the [notes](#hints-on-the-server-software) for more.)
6. Manage SSH key access for the Pis to the user SSH.
7. Restart.

### Hints
- Server files are in `/var/doccam-server`
- Nginx uses dafault config file.
- Master Server SSH-Port: 54533
- letsencrypt is in `/opt/letsencrypt`
- control the server service `systemctl [start|stop|restart|status] doccam-server`
- SSH tunnel user: `ssh`, Password: `secretalbatross`
- Server user: `doccam-serv` (no login)
- The self signet certificate is in `/etc/ssl/self`
- The image runs `ARCH Linux`
