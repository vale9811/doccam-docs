# Doccam-Docs

The documentation of the Department of Conservation (New Zealand) live streaming setup.

Hosted on: https://docstuff.protagon.space

## Building

 - Dependencies: `mkdocs, mkdocs-alabaster` available throug pypi
 - Compiling: just run `mkdocs` in the project root, the rendered html
   docs will be in `site`

