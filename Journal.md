[TOC]
# Journal
Without knowing **anything** I did some research about the way things **might be working**.

## 1. Camera to YouTube
The IP-Camera provides a live stream over the RTSP protocol. One could just embed this into a web player to publish it. But because bandwidth is limited, the stream must be restreamed over the YouTube server (which also allows some goodies like scrolling back in time up to 4h and recording). In order to do so the RTSP stream gets transcoded to a RTMP stream (the protocol which YouTube is using) via the raspberry pi and some kind of software (ffmpeg I assume, check https://trac.ffmpeg.org/wiki/StreamingGuide). The actual encoding is done by the Camera itself, therefore the performance of a raspberry pi is suitable cause it's only translating the protocols and does not encode the video. (`-codec copy`)

One command that can be used is:
```
ffmpeg -i rtsp://[camera-ip]/profile -codec copy -f flv rtmp://[YouTube-server]/[key]
```

I did some experiments on my local computer streaming an RTSP stream from my phone to YouTube. It worked but lagged due to the internet connection.

### 1.1 Possible Issues (and Solutions)
The IP camera has probably a hardware encoder. The Raspberry Pi must cause the problems.
- camera stream lags from time to time / short blackouts -> ffmpeg crashes / stops 
 - Solution: maybe make ffmpeg more tolerant
- Arch Linux ARM instead of raspbian
- manage Internet drop-out 

An overall Solution could be to create a service that manages the ffmpeg process and restarts it once crashed due power cut, internet cut etc.

## 2. Control Panel
In order to control and maintain the Raspberry-Pi's more easily a web-interface can be developed. There are two concept in my mind. Both rely on a server/client model but there are some differences in the distribution of tasks. The client parts of both solutions are in charge of supervising the transcoding software (ffmpeg). shaBut in other aspects they differ.

Both solutions will be capable of controlling the streaming-software, create YouTube-streams automatically and show statistics and logs to the user.

### 2.1 Concept One
#### Client
- hasn't an own Web-Interface
- get's instructions from the master server
- shares statistics and configuration info with the server

#### Server
- caches all the info from the clients (reduces the load on the Raspberry Pi's)
- hosts the web interface
- communicates with the YouTube API for creating streams and getting statistics
- talks to the Raspberry Pi's to configure them
- maybe provides a ssh-tunnel

#### Conclusion
##### Pro
- performance efficient for the Raspberry Pi's (which do the critical tasks)
- easy in access

##### Con
- would rely on one single server for configuration (can't be configured directly from the Raspberry Pi due to the lack of a client side)
- a bit more complex

### 2.1 Concept Two
Basically shifts most of the server tasks to the clients.

#### Client
- has it's own web interface (like the server in concept one)
- does all the configuration
- communicates with YouTube API

#### Server
- only shows all PI's available and forwards to the individual web-interfaces

#### Conclusion
##### Pro
- simpler

##### Con
- more performance intense for the Raspberry Pi's

## Desired workflow for setting a new camera up
1. Wire up the hardware.
2. Configure the networking. - can it be optimised?
3. Power on the PI.
4. Configure the stream through the web interface and start it.
5. Done :).


## Multiple Streams at Once
Have a look at Mutli-Camera Events: https://support.google.com/YouTube/answer/2853812?hl=en.

## Hardware Suggestions
See: https://www.raspberrypi.org/help/faqs/

- Power-Supply: 1.2A (for Raspberry Pi 2), 2.5A (For Raspberry Pi 3 or Pi with USV, 1.2A will do the trick too as we don't require power from the Pi's USB ports)
- SD-Card: 8gb (like [this](https://www.pbtech.co.nz/index.php?z=p&p=MEMSDK3211&name=SanDisk-8GB-Mobile-Ultra-microSDHC-up-to-48MBs-CLA) one) - just NO no-name -> SanDisk, Samsung etc. are OK
- Raspberry PI 2/3 -> wouldn't make a difference, but this needs to proved in tests
- Backup Battery: Just a USB Power Bank https://www.youtube.com/watch?v=qev1E7bqNjA
- maybe openwrt router instead of raspi
- 4G modem

# Questions
1.  Will there be an updated version of the document (priorities) that Matt sent me and is the current version still valid?
2.  Rangi didn’t respond to me - will he, which info will be provided - should i write the whole current documentation (which is ok)?
3.  Which type of work-time-record would you prefer.
4.  Which type of hardware will and can be provided.
5.  How should i document my progress: I could share my concept paper on my Webserver and email you when sth. changes. Or set up a Repository on my Version-Management Server. (which auto-notifies people).
6. Should i follow a priority list?
7. Should i work at a/the office (which would be cool).
8. bitrate
9. how does the internet connection work
10. which command
11. Recording?

# Software setup on Raspberry PI
- Installed ArchLinuxARM
- Run system update
- User login:
  - Username: alarm
  - Password: TitRPufsTY (This is the Raspberry Pi used for streaming to YouTube)
- Root login: disabled, use `sudo bash` 
- Build ffmpeg with librtmp support

#Research
- researched about traffic shaping

##ffmpeg
- `-rtmp_flush_interval` maybe more packets in one request to minimize problems / less to minimize retransmission

#solutions
- run ffmpeg as background service (screen/forever)

