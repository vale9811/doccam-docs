# Hardware Setup
<figure>
![Diagaram](img/current_setup_new.svg)
<figcaption>Fig. 1 - Diagram of the current hardware setup and data flow.</figcaption>
</figure>

The above diagram shows the basic setup and functionality of the streaing solution. The **IP Camera** Provides an encoded/compressed live video stream which's protocol isn't supported by YouTube. Therefore the **Raspberry PI** grabs the stream from the network (through the router) and translates (transcodes) the stream into a protocol that Youtube understands. This is relatively light work. Finally the **PI** streams the video to youtube (again through the router, over the internet).The user will be able to receive the Stream through YouTube.

Controlling this process by accessing the raspberry pi directly is difficult and not always possible because of the lack of access to ISP provided Routers (no port-forwarding).
Therefore software controls the encoding process (and restarts it if necessary) and enables easier access to the raspberry pi (even behind a firewall) by connecting to a master-server (by SSH and WebSockets). The master-server will manage the connections to the Raspberry Pi's and host a web-interface. The staff will be able to control the Raspberry PI and the streaming-software remotely through the web interface on the Master-Server. An advanced control is given through a SSH-Tunnel (Command Prompt) for direct access to the Raspberry Pi and the IP-Camera's own Web-Interface. In the end, it will not to be distinguished from direct acces to the pi (Tunnel), which makes acces easier.
That's the basic workflow and purpose of the whole setup. The following chapter will explain the details of the current setup. A setup guide and a concrete hardware recommendations will follow after this chapter.

## IP Camera
The IP Camera is the most important part of the setup. It must be suitable for outdoor (IP66) conditions and provide a *H.264* encodet video-stream through a network connection.

### Connections
The camera is connected to the router by **Ethernet**. To power the camera either a PoE (**Power over Internet**) enabled router is needed or a PoE adapter is hooked up between the camera and the router.

### The IP-Rating and which Rating to Choose
> The IP Code, International Protection Marking, IEC standard 60529, sometimes interpreted as Ingress Protection Marking, classifies and rates the degree of protection provided against intrusion (body parts such as hands and fingers), dust, accidental contact, and water by mechanical casings and electrical enclosures. (for more information: [Wikipedia](https://en.wikipedia.org/wiki/IP_Code))

For outdoor cameras like the "Royal Cam" require at least a rating of IP66. This means that it is dust proof and dust will not harm the camera even if it's not dust tight (first digit). A IP66 rated Camera can also resist "Powerful water jets" from any direction and should be rain-proof.

### Encoding
The IP Camera has a special hardware encoder built in. The Raspberry PI would be to weak to encode the stream. Encoding must be done to compress the stream to fit the bandwidth of the internet connection. The camera **must** encode the video with the **H.264** format/codec, which is required by YouTube. Most provide their streams over the **RTSP** protocol.

One should choose a camera, which encodes the aduio as `acc` or `mp3` natively at a sample rate of `44100Hz/48000Hz/96000Hz`.

## Router/Modem
The router manages the connections between the camera and the Raspberry PI as well as the Internet Connection of the Raspberry Pi. Because it is provided by the ISP, it will not allways be accessible for specieal configuration.

### Basic Hardware Requirements
The router must feature:

- At least 2 LAN ports
- An Internet modem (Radio/DSL/Fiber...)

Desirable features:

- Power over Internet (PoE) support
- USB port to power the raspberry pi
- some kind of pre-configuration to give all members of the local networks static ip
- Gigabit LAN and good routing performance (**needs to be benchmarked**)

### Internet Connection
The internet connection should feature:
- A static IP
- At least an uplink bandwidth of: 5-10Mbit/s
- The firewall should allow outgoing SSH/HTTP/RTMP connections (most, when not all routers do that)

### Local Network
> The local network (LAN - Local Area Network) is provided by the router. Therefore it is easier to configure as the global network (WAN - Wide Area Network) which is provided by the ISP (Internet Service Provider).

- *All members of the local network (cameras, rapsberry pi, ...) should have static IPs*

## Raspberry PI
The Raspberry PI translates (transcodes) the RTSP of the Camera into RTMP protocol and transmits it to YouTube. This isn't a very performance intense task, therefore a cheap computer like the Raspberry Pi is suitable.

### Which Model to Choose
There are currently 3 Generations of Raspberry PI's available. It's always preferable to choose the most recent one, because it's the cheapest (because still in production). A Raspberry PI of the 2nd generation is also suitable if it's cheaper. However any version would have enough performance.

**To use the premade image a Raspberry Pi v2 is required.**

### Connections and accessories
The PI is connected to the Router/Internet by Ethernet.

Additional hardware / accessories required:

- Power supply: Ampere | Router with USB port (0.9 - A)
- SD-Card: 8GiB are enough, reliable brand (no no-name) like SAMSUNG, SanDisk, Kingston

## UPS
A Uninterruptible power source is a device that features a battery which is charged when not used. If there is a power cut the UPS will power the devies (Pi, Router, Camera).
