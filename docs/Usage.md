This is a usage guide for the webinterface of the server software which you can access: [here](https://cams.doc.govt.nz/).

When you open the web interface, you will see a list of all Raspberry Pis known to the Master-Server. Each item of the list offers the same functionality. In the following this functionality will be explained.

## Menu Bar
The Menu Bar feautures a filter to search in the list of available Raspberry Pis. To search enter the search term into the text box an click 'FILTER'. To see all Pis again click 'SHOW ALL'.

## Status Bar
The status bar consists of the name of the Pi and one to three status labels.
When the Raspberry Pi isn't connected to the Master-Server the first label will be red and indicate 'Offline' and all functions will be locked until the Pi comes online again. Otherwise this status label will indicate 'Online' (green).
The second label indicates wether the the stream to YouTube runs (green, 'Streaming') or not (orange, 'paused'). A third red label will only appear if there is an error (see [this](#errors)).

## Buttons
| Button             | Description                                                                                                                                                                                                                                                                                                          |
|--------------------|----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Start/Stop         | Start and stop the stream.                                                                                                                                                                                                                                                                                           |
| Restart            | Restart the stream.                                                                                                                                                                                                                                                                                                  |
| View Logs          | View the logs of the pi.                                                                                                                                                                                                                                                                                             |
| Snapshot           | Creates a snapshot from the camera image.                                                                                                                                                                                                                                                                            |
| Configuration      | Configure the Pi.                                                                                                                                                                                                                                                                                                    |
| Restart SSH Tunnel | Restarts the SSH tunnel between the Pi and the Master-Server manually. Use this function if the Cam-Panel or the SSH-Access doen't work.                                                                                                                                                                             |
| Cam-Panel          | Access the Web-Interface of the IP Camera. If you want to bookmark the camera panel, you should drag this button into your bookmarks bar. **The cam panel is secrured by a self signet certificate. If you open the site, the broweser will display an error message. Ignore this message and click on 'continue'.** |
| SSH-Acces          | **Only for advanced users.** Displays information about how to connect dirctly to the raspberry pi via SSH.                                                                                                                                                                                                          |



## Logs
This function displays the logs of the Pi, color-coded by importance.

| Color  | Importance                                                          |
|--------|---------------------------------------------------------------------|
| White  | Misc                                                                |
| Green  | Successfull start of the transcoder (ffmpeg)                        |
| Blue   | Information about user commands. Star/Stop/Restart etc.             |
| Orange | Crash of the encoder. Displays the last line of the command output. |
| Red    | Error. Displays the errors which occured.                           |


## Errors
If a known error occurs (e.g. an error with the hardware or the internet connection), this will displayed in the status bar. Some functions of the Pi will be blocked until the error is solved.

| Error                    | Descriptopn                                            | Possible Solution                                                                                                                                             |
|--------------------------|--------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------------------------|
| Camera Disconnected      | The software can't connect to the camera.              | Enter the correct camera ip in the configuration. / Check the networking hardware. / Wait for the camera to come back online (the software will detect this). |
| YoutTube Disconnected    | YouTube is offline or there is no internet connection. | Wait until the software reconnects.                                                                                                                           |
| Wrong ffmpeg executable. | The Pi can't find the ffmpeg executable.               | Correct the ffmpeg patrh in the Settings.                                                                                                                     |


## Configuration
The configuration is self-descriping. Don't change the name of the camera if it isn't necesarry. (Key collumn only for the `config.js` file.)

| Key                 | Setting                  | Description                                                             |
|---------------------|--------------------------|-------------------------------------------------------------------------|
| name                | Name                     | Name of the station. Change with Caution!                               |
| camIP               | Camera IP                | IP address of the camera.                                               |
| camPort             | Camera Port              | Port of the RTSP stream of the camera.                                  |
| camPanelPort        | Camera Webinterface Port | Port of the Webinterface of the camera.                                 |
| camProfile          | Streaming Profile        | Camera profile for streaming.                                           |
| snapProfile         | Snapshot Profile         | Camera profile for snaphots.                                            |
| key                 | Stream-Key               | Youtube streaming-keys.                                                 |
| master              | Master-Server URL        | URL of the master-server. Change with caution!                          |
| ffmpegPath          | FFMPEG Path              | Path to ffmpeg. Change with caution!                                    |
| customOutputOptions | Custon FFMPEG flags      | Custom flags for ffmpeg (audio, etc...). Flags must be comma separated! |
| ssh-user            | SSH User                 | SSH user for the master Server.                                         |
| ssh-port            | SSH Port                 | SSH port for the master Server.                                         |
| ssh-local-user      | Local SSH User           | SSH user for the machine on which this is running.                      |
