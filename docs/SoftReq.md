## Raspberry Pi
The preferable operating system [Arch Linux ARM](https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-3) is lightweight and ships without any additional software (though new software can easily be installed). Arch Linux improves the performance of the Raspberry PI and is therefore optimal for setup.
After setting up the first PI, his SD-Card could be cloned to set-up new ones and as backup in the case of hardware failure.
For a guide which shows how to set the client software up see **[this](Guide.md)**.

### Specieal Requirements
- An Internet Connection which allows outgoing `HTTP` and `SSH` traffic.

## Master Server
The server can have a quite small performance. Suitable is an AWS micro instance ([see this for more](Guide.md#aws-image-current-installation)).

Also needet are:

- A domain name: `exaple.com`
- A wildcard domain for all subdomains: `*.example.com`

## Client Requirements
- A modern Web-Browser with Websocket support: Google Chrome, Mozilla Firefox, Microsoft Edge (not Internet Explorer)
- A SSH client: PuTTY for Windows, no additional software on MacOS or Linux
