# Introduction

This is the documentation for a live streaming setup (like the one
used for the [RoyalCam](https://www.doc.govt.nz/royalcam)), which
publishes a video feed from a off-the-shelf IP-Camera to YouTube. The
following Documentations covers everything from preparation to setup
which is needet to replicate the setup used for the RoyalCam.


> **Note**: This documentation now is 3 years old and has grown a
> little outdated in the parts concerning the available features and
> the operation of the software in use. It really needs some
> TLC. Nevertheless, the student of the following will still be
> rewarded with some understanding of the basics.

If questions should arise, pop (or to be precise: smtp, if you know
what I mean) an email at `hiro (at) protagon.space`.

## Some Useful Links

 - [source of this very
   documentation](https://gitlab.com/vale9811/doccam-docs/)
 - [source of the client side
   software](https://gitlab.com/vale9811/doccam-pi)
 - [source of the server side
   software](https://gitlab.com/vale9811/doccam-server)
 - [source of the server side docker
   deployment](https://gitlab.com/vale9811/doccam-server-docker)
 - [source of a script to generate fresh raspberry-pi images with the
   client software
   preinstalled](https://gitlab.com/vale9811/doccam-pi-image-maker)
   (runs on arch linux, only for the adventurous):

# The Purpose of the Setup

Despite providing a Video-Stream which is easily accessible in the
local network the video data from a typical IP Camera can't be
directly published. The reason for this lies within the architecture
of the internet. If a client accesses the stream, the data gets sent
through physical connections until it arrives at his computer. If
multiple clients accessing the stream simultaneously, the data get's
sent multiple times because the geographical location of the clients
differ. This results in an increase of the bandwidth usage of the
stream provider. Because it would be really expensive and ineffective
to host every single camera through an internet connection with a huge
bandwidth the stream get's sent to a relay server which provides the
stream to the individual users. (Furthermore there is the litte issue
of the camera sitting behind a NAT router.) Besides being a **free**
service, youtube offers the features of a social network and more
(analytics, replaying older parts of the stream). Therefore YouTube is
the optimal relay server. Youtube and other similar service don't
support the RTSP protocol which the camera is using (because it is
flowing in the "wrong direction"). Therefore a computer (the cheapest
reliable solution is a raspberry pi) must transcode (translate) the
stream into the RTMP protocol which YouTube understands. That's
basically the reason why we need such a (rather complex) setup. A
frontend software should control this process and provide easy
management access.
