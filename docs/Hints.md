This Appendix provides some Questions and Answers as well as general Hints.

# Q&A

*Not sure how to disable the profile – I didn't see this option in the camera interface.*

- You cant disable a default (1-3) Profile. Its best practice to just leave them as they reflect the hardware capabilities and should work in parallel in any configuration. But as far as i know, they're only active when you access them. We only access two (Profile 1 for Snapshots/Recording (higher Quality) and Profile 3 (Streaming)).

*What I'm unsure of, is if we edit a profile via the camera interface, do we then then to edit it within the Pi too? I thought we did.*

- (Very) Short Answer: No.
- Long Answer: The pi only needs to know which stream it should access, the rest is completely up to the camera encoder. If you change something in the cam settings there's no need to change the pi settings as long as the stream url (The 'path' which the pi accesses) does not change. When you somehow change the stream path, then you need to change the pi settings. The pi will indicate that as error (check the docs).


## The current camera

The interface of the current camera provides up to four Profiles of which two (1, 3) can be configured to have a HD (`1920x1080`) resolution. You can manage the profiles in the webinterface under: `Basic Setup/Video/` in the `Profile` tab.

As in most cameras you've got the posibility to change resolution, encoding (VBR, CBR, Bitrate), framerate and audio Settings. This should be similar in most other systems.

## Framerate

The **framerate** tells how many individual pictures are there in one second of the stream.
A high framerate will consume more **Bandwidth** but makes the stream look smoother.

A framerate of 24 Pictures per Seconds (Frames per Second, FPS) is optimal, as it is the lowest Framerate to be recognized as a smooth motion picture.  

## Bitrate

The **Bitrate** tells of how many Bits one second of the stream consists. So for example `4 Bit/s` means `4 Bit per second`. The Prefixes K(ilo), M(ega) and G(iga) mean that the value gets multiplied by 2^10, 2^20 and 2^30.

## Badnwidth

The **Bandwidth** tells how many Bits per second can be sent by the internet connection. See [Bitrate](#bitrate) for the terminology.

## Encoding

A video stream consists of a series of pictures. Each picture has a **resolution** and a certain size. To reduce the overall size of the stream and save bandwidth, the stream is being encoded. Encoding means compressing the series of images until through an algorithm which can vary (we prefer H264).
The bandwidth usage of a stream is described by it's **Bitrate**. The Encoder compresses the stream to a target **Bitrate** which decreases the visueal quality the lower the **bitrate** is and the higher the **resolution** is.

A good bitrate for a full-hd (`1920x1080`) is `4 Mbit/s`.

You can choose between a fixed bitrate and a target quality with a maximum bitrate (by analysing changes in the image) where the latter should be preferred for saving bandwidth. The fixed bitrate should be used for one-picture Snapshots as we only have one frame which **must** have a good quality and we dont need to worry about bandwidth.

Setting a too high **Bitrate** will cause a breaking stream which doesn't fit into the upload bandwidth. A too low **Bitrate** will result in poor image quality.

## Resolution

The video Resolution determines of how many Pixels the image consists and how much detail it shows. It only has an indirect influence on the quality of the stream (See encoding...).

Basically you may want to set it as high as possible to achieve a maximum of detail.
